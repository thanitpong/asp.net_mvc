﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workshop_MVC.Context;

namespace Workshop_MVC.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        db_testEntities dbobj = new db_testEntities();
        public ActionResult Users(tb_user obj)
        {
            return View(obj);
        }

        [HttpPost]
        public ActionResult AddUsers(tb_user model)
        {
            if (ModelState.IsValid)
            {
                tb_user obj = new tb_user();
                obj.ID = model.ID;
                obj.Fname = model.Fname;
                obj.Lname = model.Lname;
                obj.Email = model.Email;
                obj.Mobile = model.Mobile;
                obj.Desc = model.Desc;
                if(model.ID == 0)
                {
                    dbobj.tb_user.Add(obj);
                    dbobj.SaveChanges();
                }
                else
                {
                    dbobj.Entry(obj).State = EntityState.Modified;
                    dbobj.SaveChanges();
                }
            }

            var list = dbobj.tb_user.ToList();
            return View("Userslist", list);

        }

        public ActionResult Userslist()
        {
            var res = dbobj.tb_user.ToList();
            return View(res);
        }


        public ActionResult Delete(int id)
        {
            var res = dbobj.tb_user.Where(x => x.ID == id).First();
            dbobj.tb_user.Remove(res);
            dbobj.SaveChanges();

            var list  = dbobj.tb_user.ToList();
            return View("Userslist",list);

        }

    }
}